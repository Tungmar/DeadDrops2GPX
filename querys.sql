CREATE TABLE deaddrops_history AS SELECT * FROM deaddrops;

CREATE TRIGGER history_for_deaddrops BEFORE UPDATE ON deaddrops
 BEGIN
  INSERT INTO deaddrops_history
   SELECT * FROM deaddrops WHERE id=old.id;
 END;

SELECT p.lastRun, d.*
FROM deaddrops d
 LEFT JOIN deaddrop_pages p USING (id)
WHERE id = 261
UNION ALL
SELECT '-', d.*
FROM deaddrops_history d
 LEFT JOIN deaddrop_pages p USING (id)
WHERE page_id NOT NULL AND id = 261
ORDER BY updateCount DESC;

SELECT * FROM deaddrops d
WHERE 1=1
 AND country = 'FR'
 AND status = 'working'
 -- AND id = 118
 -- AND about LIKE '%&%'
ORDER BY statusUpdated DESC;

UPDATE deaddrop_pages SET deadDrop_id = 1864 WHERE id = 1864;
INSERT INTO deaddrops (id, page_id, firstStored, lastUpdated, updateCount, name, type, size, longitude, latitude, address, country, created, statusUpdated, status, about)
VALUES (1864, 1864, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 0, 'testDrop', 'USB drop', '0 byte', 40, 40, 'non existing', 'US', CURRENT_DATE, CURRENT_TIMESTAMP, 'working', 'This is just a test');
