DeadDrops2GPX
=============

This script will download the Dead Drops descriptions from
https://deaddrops.com/db and generate GPX files. This GPX files can be used to
find the Dead Drops whit various (GPS) navigation systems including some which
are optimized for GeoCaching as the waypoints for the Dead Drop locations are
stored like GeoCaches.


# Installation

1. Install PHP 7.1 including SQLite and Intl extension.
2. Install composer
3. Get the complete source of this script and switch to the main directory
4. Run

    composer install

5. Create database whit

    vendor/bin/doctrine orm:schema-tool:create


# Execution

Run

    php DeadDrop2GPX.php

After a successfull run you have a GPX file for each country whit all active
Dead Drops in the GPX folder.

Have fun!


# License

This program is free software. It comes without any warranty, to the extent
permitted by applicable law. No function is garanted and your fully responsible
if you use it. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See http://www.wtfpl.net/ for more details.

For license information about the Dead Drops and their descriptions see
https://deaddrops.com
