<?php
require_once "bootstrap.php";

$baseURL = 'https://deaddrops.com/db/?page=view&id=';

$crawler = new DeadDropCrawler($entityManager);
$crawler->setBaseURL($baseURL);
$crawler->setMaxSkip(20);

if($argv[1]){
    $crawler->crawlWhitId((int) $argv[1]);
}
else {
    $crawler->crawlAllNew();
}

$exporter = new DeadDropExporter($entityManager);
$exporter->setDirectory(__DIR__ . '/GPX/');
$exporter->createGpxByCountry();
