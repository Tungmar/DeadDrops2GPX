<?php

use Doctrine\ORM\EntityManager;

class DeadDropExporter{

    protected $entityManager;
    protected $gpxWriter;

    protected $directory;

    public function __construct(EntityManager $entityManager){
        $this->entityManager = $entityManager;
        $this->gpxWriter = new QDGpxWriter();
    }

    public function createGpxByCountry(){
        $countries = $this->getCountries();
        $countriesCount = count($countries);
        $dropsCount = 0;
        echo "Creating a GPX file for each of the $countriesCount countries whit active Dead Drops.\n";
        echo $countriesCount ? 'Now creating ' : 'That means there is nothing to do...';

        foreach($countries as $country){
            echo "$country[code]:$country[active] ";
            $dropsCount += $country['active'];
            $dql = "SELECT d "
                 . "FROM DeadDrop d "
                 . "WHERE d.status = 'working'"
                 . " AND d.country = :country";
            $query = $this->entityManager->createQuery($dql);
            $query->setParameter('country', $country['code']);
            $this->createGPX('DeadDrops_'.$country['code'].'.gpx', $query->getResult());
        }
        echo "\nTotal $dropsCount Dead Drops written. Have fun!\n";
    }

    public function getCountries(){
        $dql = "SELECT d.country code, COUNT(d.id) active "
             . "FROM DeadDrop d "
             . "WHERE d.status = 'working'"
             . "GROUP BY d.country "
             . "ORDER BY d.country DESC";
        return $this->entityManager->createQuery($dql)->getArrayResult();
    }

    protected function createGPX($file, $deadDrops){
        $this->gpxWriter->startFile($this->directory.$file);
        foreach($deadDrops as $deadDrop){
            $this->gpxWriter->addCache($deadDrop);
        }
        $this->gpxWriter->closeFile();
    }

    public function setDirectory(string $directory){
        if(!is_dir($directory) || !is_writable($directory)){
            throw new Exception("Can not write to [$directory].");
        }
        $this->directory = $directory;
    }

}
