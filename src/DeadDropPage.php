<?php
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="deaddrop_pages", indexes={@ORM\Index(name="lastRun_index", columns={"lastRun"})})
 */
class DeadDropPage{

    /**
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     *
     * @ORM\OneToOne(targetEntity="DeadDrop", cascade={"persist"})
     */
    private $deadDrop;

    /**
     * Date when this was last checked
     * @ORM\Column(type="datetimetz")
     */
    private $lastRun;

    public function __construct($id){
        $this->id = $id;
    }

    public function setDeadDrop($deadDrop){
        $this->lastRun = $deadDrop->getFirstStored();
        $this->deadDrop = $deadDrop;
    }

    public function run(){
        $this->lastRun = new \DateTime('now');
    }
}
