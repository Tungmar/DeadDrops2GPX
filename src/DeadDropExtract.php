<?php

class DeadDropExtract{

    protected static $grabList = [
        'id'            => "//td[b[.='Id: ']]/following-sibling::td",
        'name'          => "//td[b[.='Name: ']]/following-sibling::td",
        'type'          => "//td[b[.='Drop-Type: ']]/following-sibling::td",
        'size'          => "//td[b[.='Size: ']]/following-sibling::td",
        'coordinates'   => "//td[b[.='Coordinates: ']]/following-sibling::td/a",
        'address'       => "//td[b[.='Address: ']]/following-sibling::td",
        'created'       => "//td[b[.='Date Created: ']]/following-sibling::td",
        'status'        => "//td[contains(text(), 'Status was set to ')]",
        'about'         => "//tr[td[b[.='About']]]/following-sibling::tr/td",
    ];

    protected static $invalid = "//body[text()[contains(.,'ERROR')]]";

    public static function getGrabList(){
        return self::$grabList;
    }

    public function checkValid(Symfony\Component\DomCrawler\Crawler $crawler){
        return ! (bool) $crawler->filterXPath(self::$invalid)->count();
    }

    public function extract(Symfony\Component\DomCrawler\Crawler $crawler){
        foreach(self::$grabList as $key => $rules){
            $this->$key = $crawler->filterXPath($rules)->html();
            $this->$key = html_entity_decode($this->$key, null, 'UTF-8');
        }

        $this->calculateCoordinate();
        $this->calcualteAddress();
        $this->calculateCreated();
        $this->calculateStatus();
        $this->calculateAbout();
    }

    protected function calculateCoordinate(){
        $match = [];
        if(!preg_match("/^([0-9]{1,3}\.[0-9]*?) ([NS])  ([0-9]{1,3}\.[0-9]*?) ([WE])$/i", $this->coordinates, $match)){
            throw new Exception("Unknown coordinates extract [$this->coordinates].");
        }
        $this->longitude = ($match[4]=='W'?'-':'').$match[3];
        $this->latitude  = ($match[2]=='S'?'-':'').$match[1];
    }

    protected function calcualteAddress(){
        // Special case for 806
        if($this->address == 'Obere Bergstrasse,  -> Berg Brauerei '){
            $this->address .= '<-, Wallerstein Bayern, DE';
        }

        $match = [];
        if(!preg_match("/^(.*?), ([A-Z]{1,2})$/i", trim($this->address), $match)){
            throw new Exception("Unknown address extract [$this->address].");
        }
        $this->address = $match[1];
        $this->country = $match[2];

        // Special case for 118
        if($this->country == 'F'){
            $this->country = 'FR';
        }
    }

    protected function calculateCreated(){
        $this->created = new DateTime($this->created);
    }

    protected function calculateStatus(){
        if(strpos($this->status, 'Status was set to') !== 0){
            throw new Exception("Unknown status extract [$this->status].");
        }
        $this->statusUpdated = new DateTime(substr($this->status, -25));
        $this->status = preg_replace('/(.*)<em>(.*)<\/em>(.*)/sm', '\2', $this->status);
    }

    protected function calculateAbout(){
        $this->about = str_replace("\r", '', preg_replace('#<br\s*/?>#i', "\n", $this->about));
    }

}
