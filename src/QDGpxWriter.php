<?php

/**
 * Quick and Dirty GPX Writer
 */
class QDGpxWriter{

    protected $file;
    protected $cacheCount = 0;
    protected $creationTime;

    protected $head = '<?xml version="1.0" encoding="utf-8"?>
<gpx xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" version="1.0" creator="Opencaching.de - https://www.opencaching.de/" xsi:schemaLocation="http://www.topografix.com/GPX/1/0 http://www.topografix.com/GPX/1/0/gpx.xsd http://www.groundspeak.com/cache/1/0/1 http://www.groundspeak.com/cache/1/0/1/cache.xsd https://github.com/opencaching/gpx-extension-v1 https://raw.githubusercontent.com/opencaching/gpx-extension-v1/master/schema.xsd http://www.gsak.net/xmlv1/4 http://www.gsak.net/xmlv1/4/gsak.xsd" xmlns="http://www.topografix.com/GPX/1/0">
  <name>Cache listing generated from Deaddrops.com</name>
  <desc>This is a waypoint file generated from Deaddrops.com</desc>
  <author>DeadDrops2GPX</author>
  <url>https://deaddrops.com</url>
  <urlname>Un-cloud your files in cement! \'Dead Drops’ is an anonymous, offline, peer to peer file-sharing network in public space.</urlname>
  <time>[[createionTime]]</time>
  <keywords>deaddrops, cache, geocache, opencaching, waypoint</keywords>';

    protected $cache = <<<EOT
  <wpt lat="[[latitude]]" lon="[[longitude]]">
    <time>[[creationTime]]</time>
    <name>[[waypointName]]</name>
    <desc>[[name]]</desc>
    <src>[[sourceSite]]</src>
    <url>[[url]]</url>
    <urlname>[[name]]</urlname>
    <sym>[[symbol]]</sym>
    <type>[[type]]</type>
    <groundspeak:cache id="[[id]]" available="True" archived="False" xmlns:groundspeak="http://www.groundspeak.com/cache/1/0/1">
      <groundspeak:name>[[name]]</groundspeak:name>
      <groundspeak:placed_by>Unknown Dead Droper</groundspeak:placed_by>
      <groundspeak:owner id="1">Unknown Dead Droper</groundspeak:owner>
      <groundspeak:type>Unknown Cache</groundspeak:type>
      <groundspeak:container>[[size]]</groundspeak:container>
      <groundspeak:attributes>
           <groundspeak:attribute id="135" inc="1">Without GPS</groundspeak:attribute>
           <groundspeak:attribute id="51" inc="1">Special tool required</groundspeak:attribute>
           <groundspeak:attribute id="157" inc="1">Other cache type</groundspeak:attribute>
      </groundspeak:attributes>
      <groundspeak:difficulty>3</groundspeak:difficulty>
      <groundspeak:terrain>3</groundspeak:terrain>
      <groundspeak:country>[[country]]</groundspeak:country>
      <groundspeak:state></groundspeak:state>
      <groundspeak:short_description html="True">[[shortDescription]]</groundspeak:short_description>
      <groundspeak:long_description html="True">[[longDescription]]</groundspeak:long_description>
      <groundspeak:encoded_hints>[[hints]]</groundspeak:encoded_hints>
      <groundspeak:logs>
      </groundspeak:logs>
      <groundspeak:travelbugs>
      </groundspeak:travelbugs>
    </groundspeak:cache>
    <oc:cache xmlns:oc="https://github.com/opencaching/gpx-extension-v1">
      <oc:type>Other Cache</oc:type>
      <oc:size>[[size]]</oc:size>
      <oc:trip_time>1</oc:trip_time>
      <oc:requires_password>false</oc:requires_password>
      <oc:logs>
      </oc:logs>
    </oc:cache>
  </wpt>
EOT;

    protected $foot = "\n</gpx>\n";

    public function __construct(){
        $this->creationTime = (new DateTime('now'))->format('Y-m-d\TH:i:s\Z');
    }

    public function startFile($filename){
        $this->file = fopen($filename, 'w');
        $params = [
            'createionTime' => $this->creationTime,
        ];
        $this->writeTemplate($this->head, $params);
    }

    public function addCache($cache){
        $params = [];
        $params['latitude'] = $cache->getLatitude();
        $params['longitude'] = $cache->getlongitude();
        $params['creationTime'] = $cache->getCreationTime();
        $params['waypointName'] = $cache->getWaypointName();
        $params['name'] = $cache->getName();
        $params['sourceSite'] = $cache->getSourceSite();
        $params['url'] = $cache->getUrl();
        $params['symbol'] = $cache->getSymbol();
        $params['type'] = $cache->getType();
        $params['id'] = $cache->getId();
        $params['size'] = $cache->getSize();
        $params['country'] = Locale::getDisplayRegion('-'.$cache->getCountry(), 'en');
        $params['shortDescription'] = $cache->getShortDescription();
        $params['longDescription'] = $cache->getLongDescription();
        $params['hints'] = $cache->getHints();
        $this->writeTemplate($this->cache, $params);
    }

    public function closeFile(){
        $params = [];
        $this->writeTemplate($this->foot, $params);
        fclose($this->file);
    }

    protected function writeTemplate($template, Array $params){
        $keys = array_map(function ($key){return '[['.$key.']]';}, array_keys($params));
        $values = array_map(function ($value){
            return htmlspecialchars($value, ENT_XML1 | ENT_QUOTES, 'UTF-8');
        }, array_values($params));
        $output = str_replace($keys, $values, $template);
        fputs($this->file, $output);
    }
}
