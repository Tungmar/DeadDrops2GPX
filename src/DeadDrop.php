<?php
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="deaddrops")
 * @ORM\HasLifecycleCallbacks
 */
class DeadDrop{

    /**
     *
     * @ORM\Id
     * @ORM\Column(type="integer", unique=true)
     */
    private $id;

    /**
     *
     * @ORM\OneToOne(targetEntity="DeadDropPage", cascade={"persist"})
     */
    private $page;

    /**
     * Date when it was first crawled
     * @ORM\Column(type="datetimetz")
     */
    private $firstStored;

    /**
     * Date when the last time a update was crawled
     * @ORM\Column(type="datetimetz")
     */
    private $lastUpdated;

    /**
     * Count how many times updates where crawled
     * @ORM\Column(type="integer")
     */
    private $updateCount;

    /**
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $type;

    /**
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $size;

    /**
     * W-E
     * @ORM\Column(type="float", nullable=true)
     */
    private $longitude;

    /**
     * N-S
     * @ORM\Column(type="float", nullable=true)
     */
    private $latitude;

    /**
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $address;

    /**
     *
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    private $country;

    /**
     * Date according to the website
     * @ORM\Column(type="date", nullable=true)
     */
    private $created;

    /**
     * Update date according to the website
     * @ORM\Column(type="datetimetz", nullable=true)
     */
    private $statusUpdated;

    /**
     *
     * @ORM\Column(type="string")
     */
    private $status;

    /**
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $about;

    /**
     * Gets triggered only on insert
     * @ORM\PrePersist
     */
    public function onPrePersist(){
        $this->firstStored = new \DateTime('now');
        $this->lastUpdated = $this->firstStored;
        $this->updateCount = 0;
        $this->page = new DeadDropPage($this->id);
        $this->page->setDeadDrop($this);
    }

    /**
     * Gets triggered every time on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate(){
        $this->lastUpdated = new \DateTime('now');
        $this->updateCount++;
    }


    public function updateByExtract(DeadDropExtract $extract){
        $this->setId($extract->id);
        $this->setName($extract->name);
        $this->setType($extract->type);
        $this->setSize($extract->size);
        $this->setCoordinate($extract->longitude, $extract->latitude);
        $this->setAddress($extract->address, $extract->country);
        $this->setCreated($extract->created);
        $this->setStatus($extract->status, $extract->statusUpdated);
        $this->setAbout($extract->about);

        if($this->page){
            $this->page->run();
        }
    }


    public function setId($id){
        if(empty($this->id)){
            $this->id = $id;
        }
        elseif($id != $this->id){
            throw new Exception("ID to set [$id] does not match loaded DeadDrop [$this->id].");
        }
    }

    public function getId(){
        return $this->id;
    }

    public function getFirstStored(){
        return $this->firstStored;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function setType($type){
        $this->type = $type;
    }

    public function setSize($size){
        $this->size = $size;
    }

    public function setCoordinate($longitude, $latitude){
        $this->longitude = (float) $longitude;
        $this->latitude = (float) $latitude;
    }

    public function setAddress($address, $country){
        $this->address = $address;
        $this->country = $country;
    }

    public function setCreated(DateTime $created){
        if($this->created != $created){
            $this->created = $created;
        }
    }

    public function setStatus($status, DateTime $date = null){
        $this->status = $status;
        if($this->statusUpdated != $date){
            $this->statusUpdated = $date;
        }
    }

    public function setStatusRemoved(){
        $removedStatus = 'removed from database';
        if($this->status != $removedStatus){
            $this->status = $removedStatus;
            $this->statusUpdated = new DateTime('now');
        }
    }

    protected function encodeHtml($html){
        return htmlspecialchars($html, ENT_COMPAT | ENT_HTML401, 'UTF-8');
    }

    public function setAbout($about){
        $this->about = $about;
    }

    public function getName(){
        return $this->name;
    }

    public function getWaypointName(){
        return 'DD'.$this->id;
    }

    public function getType(){
        return ($this->type = 'USB drop') ? 'Geocache|Unknown Cache' : 'Geocache|Virtual Cache';;
    }

    public function getSize(){
        return 'Micro';
    }

    public function getlongitude(){
        return $this->longitude;
    }

    public function getLatitude(){
        return $this->latitude;
    }

    public function getCountry(){
        return $this->country;
    }

    public function getCreationTime($format = 'Y-m-d\TH:i:s\Z'){
        return $this->created->format($format);
    }

    public function getSourceSite(){
        return 'deaddrops.com';
    }

    public function getUrl(){
        return 'https://deaddrops.com/db/?page=view&id='.$this->id;
    }

    public function getSymbol(){
        return 'Geocache';
    }

    public function getShortDescription(){
        $out = '<p>DeadDrop, '.$this->type.' '.$this->size."<br>\n";
        $out .= 'Status "'.$this->status.'" reported at '.$this->statusUpdated->format('c')."<br>\n";
        $out .= 'Address: '.$this->encodeHtml($this->address).', '.$this->country.'</p>';
        return $out;
    }

    public function getLongDescription(){
        $out = $this->about;
        $out .= "<br>\n<br>\nSee full description at <a href=\"".$this->getUrl().'">'.$this->getUrl().'</a>';
        return $out;
    }

    public function getHints(){
        return '';
    }

}
