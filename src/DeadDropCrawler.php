<?php

use Goutte\Client;
use Doctrine\ORM\EntityManager;

class DeadDropCrawler{

    protected $entityManager;
    protected $client;

    protected $baseUrl;
    protected $maxSkip = 20;
    protected $currentSkip = 0;
    protected $totalSkip = 0;
    protected $totalNew = 0;

    public function __construct(EntityManager $entityManager){
        $this->entityManager = $entityManager;
        $this->client = new Client();
    }

    public function setBaseURL($url){
        $this->baseUrl = $url;
    }

    public function setMaxSkip($max){
        $this->maxSkip = $max;
    }

    public function crawlAllNew(){
        $id = $this->getLastId();
        echo "Crwaling new Dead Drops. The last had ID ".$id.".\nNow crawling";

        while(true){
            $id++;
            echo ' '.$id;
            try{
                if(!$this->crawlPage($id)){
                    echo '!';
                    $this->currentSkip++;
                    $this->totalSkip++;
                    if($this->maxSkip < $this->currentSkip){
                        echo "\nReached max skip. Asuming all DeadDrops are crawled.";
                        break;
                    }
                    $this->logPageInvalid($id);
                    continue;
                }
            }
            catch(Exception $e){
                echo "E\n".$e->getMessage()."\n";
            }
            $this->currentSkip = 0;
            $this->entityManager->flush();
            $this->totalNew++;
            usleep(rand(10000, 2000000));
        }
        $this->entityManager->flush();
        echo "\nCrawling New Done. Got $this->totalNew and skipped $this->totalSkip.\n";
    }

    public function crawlWhitId(int $id){
        echo "Crawling Dead Drop whit ID $id.";
        $invalid = false;
        if(!$this->crawlPage($id)){
            $this->logPageInvalid($id);
            $invalid = true;
        }
        $this->entityManager->flush();
        echo ($invalid ? ' It does not exist.' : ' Done.')."\n";
    }

    public function logPageInvalid(int $id){
        $page = $this->entityManager->find("DeadDropPage", $id);
        if(!$page){
            $page = new DeadDropPage($id);
        }
        $page->run();

        $deadDrop = $this->entityManager->find("DeadDrop", $id);
        if($deadDrop){
            $deadDrop->setStatusRemoved();
        }

        $this->entityManager->persist($page);
    }

    public function getLastId(){
        $dql = "SELECT MAX(d.id) FROM DeadDrop d";
        return (int) $this->entityManager->createQuery($dql)
              ->getSingleScalarResult();
    }

    protected function crawlPage(int $id){
        $crawler = $this->client->request('GET', $this->baseUrl.$id);
        $extract = new DeadDropExtract();

        if(!$extract->checkValid($crawler)){
            return false;
        }

        $extract->extract($crawler);

        $deadDrop = $this->entityManager->find("DeadDrop", $id);
        if(!$deadDrop){
            $deadDrop = new DeadDrop();
        }

        $deadDrop->updateByExtract($extract);
        $this->entityManager->persist($deadDrop);
        return true;
    }

}
